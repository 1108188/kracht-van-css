window.onload = () => {
	const project = document.getElementById("js--project");
	const imtd1 = document.getElementById("js--imtd1");
	const keuzevak = document.getElementById("js--keuzevak");

	const projectButton = document.getElementById("js--project-display");
	const imtd1Button = document.getElementById("js--imtd1-display");
	const keuzevakButton = document.getElementById("js--keuzevak-display");

	projectButton.addEventListener("click", function () {
		buttonClick(project);
	});

	imtd1Button.addEventListener("click", function () {
		buttonClick(imtd1);
	});

	keuzevakButton.addEventListener("click", function () {
		buttonClick(keuzevak);
	});
};
